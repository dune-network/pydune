from functools import lru_cache

from pydune.michelson.contract import Contract, ContractParameter, ContractStorage
from pydune.michelson.micheline import michelson_to_micheline

parameter_dn = """
parameter
    (or (or (nat %burn :token_id) (pair %mint (address %owner) (nat %token_id)))
        (pair %transfer (address %destination) (nat %token_id)))
"""
storage_dn = "storage (map nat address)"


class NonFungibleTokenImpl(Contract):

    @property
    @lru_cache(maxsize=None)
    def parameter(self) -> ContractParameter:
        return ContractParameter(michelson_to_micheline(parameter_dn))

    @property
    @lru_cache(maxsize=None)
    def storage(self) -> ContractStorage:
        return ContractStorage(michelson_to_micheline(storage_dn))
