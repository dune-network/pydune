from glob import glob
from os.path import abspath, dirname, join
from pprint import pprint
import fire

from pydune import pydune, Contract, RpcError
from pydune.michelson.docstring import generate_docstring
from pydune.operation.result import OperationResult
from pydune.tools.github import create_deployment, create_deployment_status

kernel_js_path = join(dirname(dirname(__file__)), 'assets', 'kernel.js')
kernel_json = {
    "argv": ['pydune', 'kernel', 'run', "-file", "{connection_file}"],
    "display_name": "Michelson",
    "language": "michelson",
    "codemirror_mode": "michelson"
}


def make_bcd_link(network, address):
    net = {
        'mainnet': 'main',
        'testnet': 'testnet'
    }
    return f'https://better-call.dev/{net[network]}/{address}'


def get_contract(path):
    if path is None:
        files = glob('*.dn')
        assert len(files) == 1
        contract = Contract.from_file(abspath(files[0]))

    elif any(map(lambda x: path.startswith(x), ['testnet', 'mainnet'])):
        network, address = path.split(':')
        pdn = pydune.using(shell=network)
        script = pdn.shell.contracts[address].script()
        contract = Contract.from_micheline(script['code'])

    else:
        contract = Contract.from_file(path)

    return contract


class PyDuneCli:

    def storage(self, action, path=None):
        """
        :param action: One of `schema`, `default`
        :param path: Path to the .dn file, or the following uri: <network>:<KT-address>
        """
        contract = get_contract(path)
        if action == 'schema':
            print(generate_docstring(contract.storage.schema, title='storage'))
        elif action == 'default':
            pprint(contract.storage.default())
        else:
            assert False, action

    def parameter(self, action, path=None):
        """
        :param action: One of `schema`
        :param path: Path to the .dn file, or the following uri: <network>:<KT-address>
        """
        contract = get_contract(path)
        if action == 'schema':
            print(generate_docstring(contract.parameter.schema, title='parameter'))
        else:
            assert False, action

    def activate(self, path, network='testnet'):
        """
        Activates and reveals key from the faucet file
        :param path: Path to the .json file downloaded from https://faucet.dune.network/
        :param network: Default is Testnet
        """
        pdn = pydune.using(key=path, shell=network)
        print(f'Activating {pdn.key.public_key_hash()} in the {network}')

        if pdn.balance() == 0:
            try:
                opg = pdn.activate_account().autofill().sign()
                print(f'Injecting activation operation:')
                pprint(opg.json_payload())
                opg.inject(_async=False)
            except RpcError as e:
                pprint(e)
                exit(-1)
            else:
                print(f'Activation succeeded! Claimed balance: {pdn.balance()} ꜩ')
        else:
            print('Already activated')

        try:
            opg = pdn.reveal().autofill().sign()
            print(f'Injecting reveal operation:')
            pprint(opg.json_payload())
            opg.inject(_async=False)
        except RpcError as e:
            pprint(e)
            exit(-1)
        else:
            print(f'Your key {pdn.key.public_key_hash()} is now active and revealed')

    def deploy(self, path, storage=None, network='carthagenet', key=None,
               github_repo_slug=None, github_oauth_token=None, dry_run=False):
        """
        Deploy contract to the specified network
        :param path: Path to the .dn file
        :param storage: Storage in JSON format (not Micheline)
        :param network:
        :param key:
        :param github_repo_slug:
        :param github_oauth_token:
        :param dry_run: Set this flag if you just want to see what would happen
        """
        pdn = pydune.using(shell=network, key=key)
        print(f'Deploying contract using {pdn.key.public_key_hash()} in the {network}')

        contract = get_contract(path)
        if storage is not None:
            storage = contract.storage.encode(storage)

        try:
            opg = pdn.origination(script=contract.script(storage=storage)).autofill().sign()
            print(f'Injecting origination operation:')
            pprint(opg.json_payload())

            if dry_run:
                pprint(opg.preapply())
                exit(0)
            else:
                opg = opg.inject(_async=False)
        except RpcError as e:
            pprint(e)
            exit(-1)
        else:
            originated_contracts = OperationResult.originated_contracts(opg)
            assert len(originated_contracts) == 1
            bcd_link = make_bcd_link(network, originated_contracts[0])
            print(f'Contract was successfully deployed: {bcd_link}')

            if github_repo_slug:
                deployment = create_deployment(github_repo_slug, github_oauth_token,
                                               environment=network)
                pprint(deployment)
                status = create_deployment_status(github_repo_slug, github_oauth_token,
                                                  deployment_id=deployment['id'],
                                                  state='success',
                                                  environment=network,
                                                  environment_url=bcd_link)
                pprint(status)


def main():
    return fire.Fire(PyDuneCli)


if __name__ == '__main__':
    main()
