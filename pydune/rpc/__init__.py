from pydune.rpc.shell import *
from pydune.rpc.protocol import *
from pydune.rpc.helpers import *
from pydune.rpc.search import *
from pydune.rpc.node import RpcNode, RpcMultiNode


class RpcProvider:

    def __init__(self, klass=RpcNode, **urls):
        self.urls = urls
        self.klass = klass

    @lru_cache(maxsize=None)
    def __getattr__(self, network) -> ShellQuery:
        return ShellQuery(node=self.klass(uri=self.urls[network], network=network))

    def __dir__(self):
        return list(super(RpcProvider, self).__dir__()) + list(self.urls.keys())

    def __repr__(self):
        res = [
            super(RpcProvider, self).__repr__(),
            '\nNetworks',
            *list(map(lambda x: f'.{x[0]}  # {x[1]}', self.urls.items()))
        ]
        return '\n'.join(res)


localhost = RpcProvider(
    sandboxnet='http://127.0.0.1:8732/'
)
dnkt = RpcProvider(
    mainnet='https://mainnet-node.dunscan.io/',
    testnet='https://testnet-node.dunscan.io/'
)
pool = RpcProvider(
    klass=RpcMultiNode,
    mainnet=[
    ],
    testnet=[
    ]
)

mainnet = dnkt.mainnet
testnet = dnkt.testnet
