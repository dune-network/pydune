from unittest import TestCase

from pydune import ContractInterface

code = """
parameter unit;
storage address;
code { DROP ;
       SENDER ;
       NIL operation ;
       PAIR }
"""
initial = 'dn1dYkLz1RvszDsPZa3qNPiD1rcotcETu3RM'
source = 'KT1WhouvVKZFH94VXj9pa8v4szvfrBwXoBUj'
sender = 'dn1fM958Z7PSPiswhtnRxLPdu7rdHff8yNhe'


class SenderContractTest(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.ci = ContractInterface.create_from(code)

    def test_sender(self):
        res = self.ci.call().result(storage=initial, source=source, sender=sender)
        self.assertEqual(sender, res.storage)

    def test_no_source(self):
        res = self.ci.call().result(storage=initial, sender=sender)
        self.assertEqual(sender, res.storage)

    def test_no_sender(self):
        res = self.ci.call().result(storage=initial, source=source)
        self.assertEqual(source, res.storage)
