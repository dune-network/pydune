from os.path import dirname, join
from unittest import TestCase

from pydune import ContractInterface, NonFungibleTokenImpl, MichelsonRuntimeError


class NftContractTest(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.nft = ContractInterface.create_from(join(dirname(__file__), 'nft.dn'), factory=NonFungibleTokenImpl)

    def test_mint(self):
        res = self.nft \
            .mint(token_id=42, owner='dn1apeTK9wFX2S1yDuFdfyzA3jHNsWB3oepU') \
            .result(storage={})
        self.assertDictEqual({42: 'dn1apeTK9wFX2S1yDuFdfyzA3jHNsWB3oepU'}, res.storage)

    def test_mint_existing(self):
        res = self.nft \
            .mint(token_id=42, owner='dn1apeTK9wFX2S1yDuFdfyzA3jHNsWB3oepU') \
            .result(storage={42: 'dn1apeTK9wFX2S1yDuFdfyzA3jHNsWB3oepU'})
        self.assertDictEqual({42: 'dn1apeTK9wFX2S1yDuFdfyzA3jHNsWB3oepU'}, res.storage)

    def test_transfer_skip(self):
        res = self.nft \
            .transfer(token_id=42, destination='dn1apeTK9wFX2S1yDuFdfyzA3jHNsWB3oepU') \
            .result(storage={42: 'dn1apeTK9wFX2S1yDuFdfyzA3jHNsWB3oepU'})
        self.assertDictEqual({42: 'dn1apeTK9wFX2S1yDuFdfyzA3jHNsWB3oepU'}, res.storage)

    def test_transfer_non_existing(self):
        with self.assertRaises(MichelsonRuntimeError):
            self.nft \
                .transfer(token_id=42, destination='dn253TkS3Yi8M88g4o5DoeMkSmJcXp4Zujqa') \
                .result(storage={}, source='dn1apeTK9wFX2S1yDuFdfyzA3jHNsWB3oepU')

    def test_transfer_unwanted(self):
        res = self.nft \
            .transfer(token_id=42, destination='dn253TkS3Yi8M88g4o5DoeMkSmJcXp4Zujqa') \
            .result(storage={42: 'dn1apeTK9wFX2S1yDuFdfyzA3jHNsWB3oepU'})
        self.assertDictEqual({42: 'dn1apeTK9wFX2S1yDuFdfyzA3jHNsWB3oepU'}, res.storage)

    def test_transfer(self):
        res = self.nft \
            .transfer(token_id=42, destination='dn253TkS3Yi8M88g4o5DoeMkSmJcXp4Zujqa') \
            .result(storage={42: 'dn1apeTK9wFX2S1yDuFdfyzA3jHNsWB3oepU'},
                    source='dn1apeTK9wFX2S1yDuFdfyzA3jHNsWB3oepU')
        self.assertDictEqual({42: 'dn253TkS3Yi8M88g4o5DoeMkSmJcXp4Zujqa'}, res.storage)

    def test_burn_unwanted(self):
        res = self.nft \
            .burn(42) \
            .result(storage={42: 'dn1apeTK9wFX2S1yDuFdfyzA3jHNsWB3oepU'})
        self.assertDictEqual({42: 'dn1apeTK9wFX2S1yDuFdfyzA3jHNsWB3oepU'}, res.storage)

    def test_burn_non_existing(self):
        with self.assertRaises(MichelsonRuntimeError):
            self.nft.burn(42).result(storage={})

    def test_burn(self):
        res = self.nft \
            .burn(42) \
            .result(storage={42: 'dn1apeTK9wFX2S1yDuFdfyzA3jHNsWB3oepU'},
                    source='dn1apeTK9wFX2S1yDuFdfyzA3jHNsWB3oepU')
        self.assertDictEqual({}, res.storage)
