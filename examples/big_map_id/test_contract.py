from os.path import join, dirname
from unittest import TestCase
from pydune import ContractInterface


class HelloWorldContractTest(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.token_v3 = ContractInterface.create_from(join(dirname(__file__), 'contract.dn'))

    def test_mint(self):
        alice = "dn1f6FmMWLkiiQKMSJq1x1DH8mDEMi7z2TRe"
        val = 3
        res = self.token_v3 \
            .mint(mintOwner=alice, mintValue=val) \
            .result(
                storage={
                    "admin": alice,
                    "balances": {  },
                    "paused": False,
                    "shareType": "APPLE",
                    "totalSupply": 0
                },
                source=alice)

        self.assertEqual(val, res.big_map_diff['balances'][alice])
